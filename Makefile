#!make
include .env

.PHONY = init dry-run deploy

dry-run:
	@cd terraform && terraform plan --var-file="../env.tfvars.json"

init: _create_env _fetch_gcloud_credentials _init_terraform

deploy: _build _terraform_apply _generate_inventory _run_ansible

destroy:
	@cd terraform && terraform destroy --var-file="../env.tfvars.json"
	rm ansible/inventory.ini

_build:
	@cd scheduler/start && zip start.zip index.js && zip start.zip package.json
	@cd scheduler/stop && zip stop.zip index.js && zip stop.zip package.json

_create_env:
	@echo ''
	@echo ' # Creating Environment Variables File'
	@echo ' #######################################'
	@echo ''
ifneq ("$(wildcard env.tfvars.json)", "")
	@echo ' - Variables File (env.tfvars.json) already exists. Delete file to recreate.'
else
	@echo ' - Variables Files did not exists. File is being created.'
	@cp env.tfvars.json.dist env.tfvars.json
	@cp .env.dist .env
	@echo ' - Variables Files (env.tfvars.json, .env) have been created.'
	@echo ' ! Please open the file and add the values. All of them are required.'
endif
	@echo ''

_fetch_gcloud_credentials:
	@echo ''
	@echo ' # Fetching gcloud credentials for terraform'
	@echo ' #############################################'
	@echo ''
ifneq ("$(wildcard terraform/account.json)", "")
	@echo ' - Credentials file (terraform/account.json) already exists. Delete file to recreate.'
else
	@echo ' > Please enter your service account: '
	@read line; gcloud iam service-accounts keys create terraform/account.json --iam-account  $$line
endif
	@echo ''

_init_terraform:
	@echo ''
	@echo ' # Initialising Terraform'
	@echo ' ##########################'
	@echo ''
	@cd terraform && terraform init
	@echo ''

_terraform_apply:
	@cd terraform && terraform apply --var-file="../env.tfvars.json"

_generate_inventory:
	@echo Generating inventory file
	@echo '[all]' > ansible/inventory.ini
	@cd terraform && terraform output -json | jq -r ".external_ip.value" | xargs -I '{}' echo '{} ansible_user=${ansible_username}' >> ../ansible/inventory.ini

_run_ansible:
	@echo Running Ansible
	cd ansible && ansible-playbook -i inventory.ini --extra-vars='{"jitsi_hostname":"${jitsi_hostname}"}' playbook.yaml
