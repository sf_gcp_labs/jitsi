# Jitsi on GCP Compute

Use this script to install a basic install of jitsi on one gcp node with optional scheduling

## Tasks

- [x] Create the underlying infrastructure with terraform
- [x] Install jitsi on the VM
- [x] Setup the scheduling
- [ ] Make the scheduling optional
- [x] Add some parameters to make things easier
- [x] Add some documentation

# Usage

Using this project is pretty straightforward and the whole process is automated with a Makefile. You'll just have to install all required tools and run 3 commands.

## Requirements

1. gcloud is installed and configured
2. terraform is installed
3. a service account in the target gcp project for terraform (note the corresponding iam-account)
4. your SSH key is authorized to access the VM on gcloud

## Installing

### 1. `make init`

Run the command `make init`. It checks for the necessary config files and creates them if they are missing. Open the file `env.tfvars.json` and add all the values for the variables. Refer to the [Configuration README](#Configuration) section for further information about the variables and more available variables.

Keep the iam service account at hand as the script will ask for it if the gcp credentials have not been fetched yet.

### 2. `make dry-run`

This step is fully optional, but this is a first test if terraform is installed and initialized correctly.

### 3. `make deploy`

Deploy the jitsi server with this command. It will create the infrastructure and install jitsi.

### 4. Add DNS entry pointing to the server

## 5. Connect via SSH to the server

Now you have to install jitsi manually and create the SSL certificate.
```
sudo apt update
sudo apt install -y jitsi-meet
sudo /usr/share/jitsi-meet/scripts/install-letsencrypt-cert.sh
```

The last command will only work if the DNS entry is working properly.

# 5. Configuration

## Terraform variables

| Name | Description | Required | Default |
|------|-------------|----------|---------|
| hostname | The hostname that will be used to open the server | [x] | |
| project | The GCP project which jitsi will be deployed into | [x] | |
| region | The GCP region that will be used | [] | europe-west3 |
| zone | The GCP zone the machine will be deployed into | [] | europe-west3-a |
| machine_type | The machine type that will be used | [] | n1-standard-1 |
| enable_scheduling | Flag to determine if scheduling should be installed | [] | true |


# Disclaimer

The code for the scheduler was not written by myself, it is straight from [Google](https://cloud.google.com/scheduler/docs/start-and-stop-compute-engine-instances-on-a-schedule?hl=de). The only issue with it: As of now, I haven't found a version for node11, so it is still using a deprecated node6. Maybe I'll find some time later to rewrite that code to node11.