data "google_compute_image" "debian_image" {
  family  = "debian-9"
  project = "debian-cloud"
}

# External IP
resource "google_compute_address" "jitsi"{
    name = "jitsi-external"
}

# VM instance
resource "google_compute_instance" "jitsi" {
    name = "jitsi"
    machine_type = var.machine_type
    zone      = var.zone
    hostname = var.hostname

    tags = ["jitsi"]

    boot_disk {
        initialize_params {
            image = data.google_compute_image.debian_image.self_link
        }
    }

    network_interface {
        network = "default"
        access_config {
            nat_ip = google_compute_address.jitsi.address
        }
    }
}

# Configure Firewall
resource "google_compute_firewall" "default" {
  name    = "jitsi"
  network = "default"

  allow {
    protocol = "udp"
    ports = [ "10000" ]
  }

  allow {
    protocol = "tcp"
    ports    = [ "80", "443" ]
  }

  source_ranges = [ "0.0.0.0/0" ]
}


######
# Scheduler related stuff.
######

# Add Pub/Sub for scheduling
resource "google_pubsub_topic" "scheduler_start_instance" {
  count = var.enable_scheduling ? 1 : 0

  name = "scheduler-start"
}

resource "google_pubsub_topic" "scheduler_stop_instance" {
  count = var.enable_scheduling ? 1 : 0

  name = "scheduler-stop"
}

# Bucket to store code
resource "google_storage_bucket" "bucket" {
  count = var.enable_scheduling ? 1 : 0

  name = "scheduler-sourcecode"
}

resource "google_storage_bucket_object" "start-code" {
  count = var.enable_scheduling ? 1 : 0

  name = "start.zip"
  bucket = google_storage_bucket.bucket[0].name
  source = "../scheduler/start/start.zip"
}

resource "google_storage_bucket_object" "stop-code" {
  count = var.enable_scheduling ? 1 : 0

  name = "stop.zip"
  bucket = google_storage_bucket.bucket[0].name
  source = "../scheduler/stop/stop.zip"
}

# Add Functions to torn instances on/off
resource "google_cloudfunctions_function" "start_instance" {
  count = var.enable_scheduling ? 1 : 0

  name = "start-instance"
  runtime = "nodejs6"
  entry_point = "startInstancePubSub"
  source_archive_bucket = google_storage_bucket.bucket[0].name
  source_archive_object = google_storage_bucket_object.start-code[0].name

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource = google_pubsub_topic.scheduler_start_instance[0].name
  }
}

resource "google_cloudfunctions_function" "stop_instance" {
  count = var.enable_scheduling ? 1 : 0

  name = "stop-instance"
  runtime = "nodejs6"
  entry_point = "stopInstancePubSub"
  source_archive_bucket = google_storage_bucket.bucket[0].name
  source_archive_object = google_storage_bucket_object.stop-code[0].name

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource = google_pubsub_topic.scheduler_stop_instance[0].name
  }
}

# scheduler jobs
resource "google_cloud_scheduler_job" "start-jitsi" {
  count = var.enable_scheduling ? 1 : 0

  name        = "start-jitsi-instance"
  description = "start jitsi instance"
  schedule    = "0 8 * * 1-5"

  pubsub_target {
    # topic.id is the topic's full resource name.
    topic_name = google_pubsub_topic.scheduler_start_instance[0].id
    data       = base64encode("{\"zone\":\"${google_compute_instance.jitsi.zone}\",\"instance\":\"${google_compute_instance.jitsi.name}\"}")
  }
}

resource "google_cloud_scheduler_job" "stop-jitsi" {
  count = var.enable_scheduling ? 1 : 0
  
  name        = "stop-jitsi-instance"
  description = "stop jitsi instance"
  schedule    = "0 15 * * 1-5"

  pubsub_target {
    # topic.id is the topic's full resource name.
    topic_name = google_pubsub_topic.scheduler_stop_instance[0].id
    data       = base64encode("{\"zone\":\"${google_compute_instance.jitsi.zone}\",\"instance\":\"${google_compute_instance.jitsi.name}\"}")
  }
}