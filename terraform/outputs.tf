output "external_ip" {
  value = google_compute_address.jitsi.address
}
