variable "hostname" {
    type = string
}

variable "project" {
    type = string
}

variable "region" {
    type = string
    default = "europe-west3"
}

variable "zone" {
    type = string
    default = "europe-west3-a"
}

variable "machine_type" {
    type = string
    default = "n1-standard-1"
}

variable "enable_scheduling" {
  description = "If set to true, enable scheduling"
  type        = bool
  default = true
}